## Integrate our code OpenAI API
import os
from constants import openai_key
from langchain.llms import OpenAI
from langchain import PromptTemplate
from langchain.chains import LLMChain

from langchain.memory import ConversationBufferMemory

from langchain.chains import SequentialChain

import streamlit as st

os.environ["OPENAI_API_KEY"]=openai_key

# streamlit framework

st.title('Indian History Event Search')
input_text=st.text_input("Search the event")

# Prompt Templates

first_input_prompt=PromptTemplate(
    input_variables=['event'],
    template="Tell me about Indian history {event}"
)

# Memory

event_memory = ConversationBufferMemory(input_key='event', memory_key='chat_history')
date_memory = ConversationBufferMemory(input_key='event', memory_key='chat_history')
descr_memory = ConversationBufferMemory(input_key='date', memory_key='description_history')

## OPENAI LLMS
llm=OpenAI(temperature=0.8)
chain=LLMChain(
    llm=llm,prompt=first_input_prompt,verbose=True,output_key='person',memory=event_memory)

# Prompt Templates

second_input_prompt=PromptTemplate(
    input_variables=['event'],
    template="when was {event} occured"
)

chain2=LLMChain(
    llm=llm,prompt=second_input_prompt,verbose=True,output_key='date',memory=date_memory)
# Prompt Templates

third_input_prompt=PromptTemplate(
    input_variables=['date'],
    template="Mention 5 major events happened around {date} in the world"
)
chain3=LLMChain(llm=llm,prompt=third_input_prompt,verbose=True,output_key='description',memory=descr_memory)
parent_chain=SequentialChain(
    chains=[chain,chain2,chain3],input_variables=['event'],output_variables=['event','date','description'],verbose=True)



if input_text:
    st.write(parent_chain({'event':input_text}))

    with st.expander('Event Name'): 
        st.info(event_memory.buffer)

    with st.expander('Major Events'): 
        st.info(descr_memory.buffer)
